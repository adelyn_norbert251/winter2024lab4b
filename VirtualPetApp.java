import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[]args){
		Scanner reader = new Scanner(System.in);
		Koala[] colony = new Koala[2];
		
		for(int i=0;i<colony.length;i++){
			
			System.out.println("Enter gender: Female or Male");
			String ColonyGender = reader.nextLine();
			System.out.println("Enter age:");
			int ColonyAge = Integer.parseInt(reader.nextLine());
			System.out.println("Enter weight:");	
			int ColonyWeight = Integer.parseInt(reader.nextLine());
			
			colony[i] = new Koala(ColonyGender,ColonyAge,ColonyWeight);
		}
		
		System.out.println("The gender of the Koala: "+colony[colony.length-1].getGender()); //before
		System.out.println("The age of the Koala: "+colony[colony.length-1].getAge());
		System.out.println("The weight of the Koala: "+colony[colony.length-1].getWeight());
		
		System.out.println("Enter gender: Female or Male"); //after
		String ColonyGenderAgain = reader.nextLine();
		colony[colony.length-1].setGender(ColonyGenderAgain);
		
		System.out.println("The gender of the Koala: "+colony[colony.length-1].getGender()); //before
		System.out.println("The age of the Koala: "+colony[colony.length-1].getAge());
		System.out.println("The weight of the Koala: "+colony[colony.length-1].getWeight());
		
		
		
	}
	
	
	
}




